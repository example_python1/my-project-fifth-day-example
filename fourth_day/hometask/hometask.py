# Модифікувати клас книги з ДЗ 3 таким чином, щоб параметри які ми передаємо
# при ініціалізації екземпляру були:
# - name, language, year - обовʼязковими
# - автори - передавались не списком, а як звичайні позиційні аргументи
# - опис книги, isbn, genres - необовʼязкові параметри і лише як ключові
# (підказки тут https://docs.python.org/3/tutorial/controlflow.html#special-parameters)
#
#
# - створіть метод у книги який повертає вік книги в роках (відносно поточного)
# (підказки тут - https://docs.python.org/3/library/datetime.html)
from typing import Optional


# клас жанр
class Genre:
    def __init__(self, name: str, description: Optional[str] = None) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"Жанр({self.name}, {self.description})"


# клас Автор
class Author:
    def __init__(
        self, first_name: str, last_name: str, year_of_birth: Optional[int] = None
    ) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def __repr__(self) -> str:
        return f"Автор({self.first_name}, {self.last_name}, {self.year_of_birth})"

    def __eq__(self, other: "Author") -> bool:
        if not isinstance(other, Author):
            raise TypeError(
                f"для типу Автор і виду {type(other)} операція не реалізована"
            )
        return (
            self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.year_of_birth == other.year_of_birth
        )

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


# клас Книга
class Book:
    def __init__(
        self,
        name: str,
        language: str,
        authors: list[Author],
        year: int,
        genres: list[Genre] = None,
        isbn: str = None,
        describe: Optional[str] = None,
    ):
        self.name = name
        self.language = language
        self.authors = authors
        self.genres = genres
        self.year = year
        self.isbn = isbn
        self.describe = describe

    def __repr__(self) -> str:
        return (f"Назва книги: {self.name}"
                f"Мова: {self.language}"
                f"Автор: {self.authors}"
                f"Жанр: {self.genres}"
                f"Рік видання: {self.year}"
                f"Опис: {self.describe}"
                f"ISBN: {self.isbn}")

    def __eq__(self, other: "Book") -> bool:
        return set(self.authors) == set(other.authors) and self.name == other.name
